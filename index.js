// alert (`B249!`)

// In JS classes can be created using the "class" keyword and {}

/*
	Naming convention: Begin with Uppercase Characters

	Syntax:
		class <Name> {
		
		}
*/

// here we have an empty student class

	class Student {
		constructor(name, email, grades)
		{
			// this.propertyName = name
			this.name = name;
			this.email = email;
			this.gradeAve = undefined;
			this.passed = undefined;
			this.passedWithHonors = undefined;
			let gradesArr = [];
			if (typeof grades === "object")
			{
				grades.forEach( grade => 
				{
					if (typeof grade === "number" && grade > 0 && grade < 101)
					{
						gradesArr.push(grade)
					}
				});
			};
			if (gradesArr.length === 4)
			{
				this.grades = gradesArr
			}
			else
			{
				this.grades = undefined
			};
		}

		login() {
			console.log(`${this.email} has logged in`);
			return this;
		}

		logout() {
			console.log(`${this.email} has logged out`);
			return this;
		}

		listGrades() {
			console.log(`${this.name}'s' quarterly grades are ${this.grades}`);
			return this;
		}
		computeAve() {
				let sum = 0;
				this.grades.forEach(grade => sum = sum + grade);
				this.gradeAve = sum/4
				return this
			}
		willPass() {
			this.passed = this.gradeAve >= 85 ? true:false;
			return this
		}

		willPassWithHonors() {
			if (this.passed)
			{
				if (this.gradeAve >= 90) 
					{this.passedWithHonors = true;
					return this
					}
				else {this.passedWithHonors = false;
					return this
					}
			}
			else
				{ 	this.passedWithHonors = undefined;
					return this;}

			return this
		}
	}

/*	Instantiation ; a process of creating objects from our class
	
	To create an object from our class use the "new" keyword, when a class has a constructor we need to supply ALL the values needed by the constructor.	
*/
// let studentOne = new Student("John", "john@mail.com");
// console.log(studentOne);


/*
	Mini exercise:
		- create a new class called "Person"
		- this person should be able to instantiate a new object with the ff fields:
			name,
			age, (should be a number and must be >= 18 otherwise set the property to underage)
			nationality,
			address

		- Instantiate 2 new objects, from the Person class
			Person1
			Person2

		- Log both objects in the console.
*/

	class Person {
		constructor(name, age, nationality, address )
		{
			// if (age >= 18 ? age : "underage") ;
			this.name = name;
			this.nationality = nationality;
			this.address = address;
			typeof age === "number" && age >=18 ? this.age = age : this.age = undefined ;		
			
			/* ALTERNATE SOLUTION
			if (typeof age === "number" && age >=18)
				{	this.age = age }
			else { this.age = undefined};*/
			
		}
	}

	let person1 = new Person ("Jane", 17, "American", "Nashville, TN");

	let person2 = new Person ("Joe", 35, "Canadian", "Vancouver");

	console.log(person1);

	console.log(person2);


/* ACTIVITY 1 */

	/* QUIZ SECTION

		1. What is the blueprint where objects are created from?
			
			ANSWER: class / classes

		2. What is the naming convention applied to classes?
			
			ANSWER: PascalCase / UppercaseWords

		3. What keyword do we use to create objects from a class?
			
			ANSWER: new

		4. What is the technical term for creating an object from a class?
			
			ANSWER: instantiation

		5. What class method dictates HOW objects will be created from that class?
			
			ANSWER: constructor () method


	*/

	/*
		FUNCTION CODING

		1. Define a grades property in our Student class that will accept an array
		of 4 numbers ranging from 0 to 100 to be its value. If the argument
		used does not satisfy all the conditions given, this property will be set
		to undefined instead.

	*/
	console.log(`with one grade higher than 100`);
	let studentOne = new Student("John", "john@mail.com", [101,84,78,88]);
	console.log(studentOne);

	console.log(`with one grade equal to 'hello'`);
	studentOne = new Student("John", "john@mail.com", ['hello',84,78,88]);
	console.log(studentOne);

	console.log(`with one missing grade `);
	studentOne = new Student("John", "john@mail.com", [84,78,88]);
	console.log(studentOne);

	console.log(`correct format`);
	studentOne = new Student("John", "john@mail.com", [89, 84,78,88]);
	console.log(studentOne);


	/*
		2. Instantiate all four students from the previous session from our
		Student class. Use the same values as before for their names, emails, and
		grades.
	*/

	let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
	console.log(studentTwo);

	let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
	console.log(studentThree);

	let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);
	console.log(studentFour);


/* PART 2 DISCUSSION : CLASS METHODS*/

	// getter & setter
		// best practice disctates that we regulate access to such properties. We do so via the use of "getters" (regulates the retrieval) and "setters" (regulates the manipulation)

	// method chaining



/* ACTIVITY 2*/

	/* QUIZ SECTION
		
		1. Should class methods be included in the class constructor?

			ANSWER: NO

		2. Can class methods be separated by commas?

			ANSWER: NO

		3. Can we update an object’s properties via dot notation?

			ANSWER: YES

		4. What do you call the methods used to regulate access to an object’s properties?

			ANSWER: getter and setter

		5. What does a method need to return in order for it to be chainable?

			ANSWER: the object

	*/

	/* FUNCTION CODING
		1. Modify the Student class to allow the willPass() and
		willPassWithHonors() methods to be chainable.

	*/

	console.log( studentOne.login().computeAve().willPass().willPassWithHonors().logout() )

	console.log( studentTwo.login().computeAve().willPass().willPassWithHonors().logout() )

	console.log( studentThree.login().computeAve().willPass().willPassWithHonors().logout() )

	console.log( studentFour.login().computeAve().willPass().willPassWithHonors().logout() )